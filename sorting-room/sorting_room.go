package sorting

import (
	"fmt"
	"strconv"
)

// DescribeNumber should return a string describing the number.
func DescribeNumber(f float64) string {
	return fmt.Sprintf("This is the number %.1f", f)
}

type NumberBox interface {
	Number() int
}

// DescribeNumberBox should return a string describing the NumberBox.
func DescribeNumberBox(nb NumberBox) string {
	f := float64(nb.Number())
	return fmt.Sprintf("This is a box containing the number %.1f", f)
}

type FancyNumber struct {
	n string
}

func (i FancyNumber) Value() string {
	return i.n
}

type FancyNumberBox interface {
	Value() string
}

// ExtractFancyNumber should return the integer value for a FancyNumber
// and 0 if any other FancyNumberBox is supplied.
func ExtractFancyNumber(fnb FancyNumberBox) int {
	switch fnb.(type) {
	case FancyNumber:
		convertedInt, err := strconv.Atoi(fnb.Value())
		if err != nil {
			return 0
		}
		return convertedInt
	default:
		return 0
	}
}

// DescribeFancyNumberBox should return a string describing the FancyNumberBox.
func DescribeFancyNumberBox(fnb FancyNumberBox) string {
	fancyNumber := ExtractFancyNumber(fnb)
	n := float64(fancyNumber)
	return fmt.Sprintf("This is a fancy box containing the number %.1f", n)
}

// DescribeAnything should return a string describing whatever it contains.
func DescribeAnything(i interface{}) string {
	switch iValue := i.(type) {
	case int:
		return DescribeNumber(float64(iValue))
	case float64:
		return DescribeNumber(iValue)
	case NumberBox:
		return DescribeNumberBox(iValue)
	case FancyNumberBox:
		return DescribeFancyNumberBox(iValue)
	default:
		return "Return to sender"
	}
}
