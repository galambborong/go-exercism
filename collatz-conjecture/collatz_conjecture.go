package collatzconjecture

import "fmt"

func CollatzConjecture(n int) (int, error) {

	err := ValidateNumber(n)
	if err != nil {
		return 0, err
	}

	steps := 0
	for n > 1 {
		if n%2 == 0 {
			n = n / 2
		} else if n%2 != 0 {
			n = n*3 + 1
		}
		steps++
	}

	return steps, nil
}

func ValidateNumber(n int) error {
	switch {
	case n == 0:
		return fmt.Errorf("input cannot be zero")
	case n < 0:
		return fmt.Errorf("input cannot be negative")
	default:
		return nil
	}
}
