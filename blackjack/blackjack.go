package blackjack

// ParseCard returns the integer value of a card following blackjack ruleset.
func ParseCard(card string) int {
	switch card {
	case "ace":
		return 11
	case "two":
		return 2
	case "three":
		return 3
	case "four":
		return 4
	case "five":
		return 5
	case "six":
		return 6
	case "seven":
		return 7
	case "eight":
		return 8
	case "nine":
		return 9
	case "ten", "jack", "queen", "king":
		return 10
	default:
		return 0
	}
}

// FirstTurn returns the decision for the first turn, given two cards of the
// player and one card of the dealer.
func FirstTurn(card1, card2, dealerCard string) string {
	dealerTotal := ParseCard(dealerCard)
	ownTotal := 0
	ownTotal += ParseCard(card1)
	ownTotal += ParseCard(card2)

	switch {
	case card1 == "ace" && card2 == "ace":
		return "P"
	case ownTotal == 21:
		switch {
		case dealerCard != "ace" && dealerTotal != 10:
			return "W"
		default:
			return "S"
		}
	case ownTotal >= 17 && ownTotal < 21:
		return "S"
	case ownTotal >= 12 && ownTotal < 17:
		switch {
		case dealerTotal >= 7:
			return "H"
		default:
			return "S"
		}
	default:
		return "H"
	}
}
