package luhn

import (
	"fmt"
	"strconv"
	"strings"
)

func Valid(id string) bool {
	fmt.Println(id)
	trimmedId := strings.ReplaceAll(id, " ", "")
	length := len(trimmedId)

	if length <= 1 {
		return false
	}

	isAlternativeIdDigit := false
	totalSum := 0

	for i := length - 1; i >= 0; i-- {
		idRune := trimmedId[i]
		idNumber, err := strconv.Atoi(string(idRune))

		if err != nil {
			return false
		}

		if isAlternativeIdDigit {
			totalSum += doubleIdDigit(idNumber)
		} else {
			totalSum += idNumber
		}

		isAlternativeIdDigit = !isAlternativeIdDigit
	}

	return totalSum%10 == 0
}

func doubleIdDigit(number int) int {
	doubledNumber := number * 2
	if doubledNumber > 9 {
		doubledNumber -= 9
	}
	return doubledNumber
}
