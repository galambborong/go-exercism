package cars

// CalculateWorkingCarsPerHour calculates how many working cars are
// produced by the assembly line every hour
func CalculateWorkingCarsPerHour(productionRate int, successRate float64) float64 {
	return float64(productionRate) * successRate / 100
}

// CalculateWorkingCarsPerMinute calculates how many working cars are
// produced by the assembly line every minute
func CalculateWorkingCarsPerMinute(productionRate int, successRate float64) int {
	const minutesPerHour = 60
	ratePerHouse := CalculateWorkingCarsPerHour(productionRate, successRate)
	return int(ratePerHouse) / minutesPerHour
}

// CalculateCost works out the cost of producing the given number of cars
func CalculateCost(carsCount int) uint {
	const batchProductionUnit = 10
	const batchProductionUnitCost = 95000
	const singleUnitCost = 10000

	excessCount := carsCount % batchProductionUnit
	batchUnitCount := (carsCount - excessCount) / 10

	totalProductionCost := excessCount*singleUnitCost + batchUnitCount*batchProductionUnitCost

	return uint(totalProductionCost)
}
