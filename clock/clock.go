package clock

import (
	"fmt"
)

type Clock struct {
	Hour   int
	Minute int
}

func New(h, m int) Clock {
	newClock := Clock{Hour: h, Minute: m}
	newClock.handleRollOver()
	return newClock
}

func (c Clock) Add(m int) Clock {
	c.Minute += m
	c.handleRollOver()
	return c
}

func (c Clock) Subtract(m int) Clock {
	c.Minute -= m
	c.handleRollOver()
	return c
}

func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.Hour, c.Minute)
}

func (c *Clock) handleRollOver() *Clock {

	for c.Minute > 59 {
		c.Hour += 1
		c.Minute -= 60
	}

	for c.Hour > 23 {
		c.Hour -= 24
	}

	for c.Minute < 0 {
		c.Minute += 60
		c.Hour -= 1
	}

	for c.Hour < 0 {
		c.Hour += 24
	}

	return c
}
