package isogram

import "strings"

func IsIsogram(word string) bool {
	if len(word) == 0 {
		return true
	}

	wordWithoutDashes := strings.ReplaceAll(word, "-", "")
	wordWithoutSpacesAndDashes := strings.ReplaceAll(wordWithoutDashes, " ", "")

	letterCount := make(map[string]int)

	for _, letter := range wordWithoutSpacesAndDashes {
		handledLetter := strings.ToUpper(string(letter))
		letterCount[handledLetter] += 1
	}

	return len(wordWithoutSpacesAndDashes) == len(letterCount)
}
