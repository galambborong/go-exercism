package letter

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// ConcurrentFrequency counts the frequency of each rune in the given strings,
// by making use of concurrency.
func ConcurrentFrequency(l []string) FreqMap {
	c := make(chan FreqMap, len(l))

	for _, line := range l {
		go func(ch chan<- FreqMap, line string) {
			ch <- Frequency(line)
		}(c, line)
	}

	mainMap := make(FreqMap, 26)
	for i := 0; i < len(l); i++ {
		for k, v := range <-c {
			mainMap[k] += v
		}
	}

	return mainMap
}
