// Package weather provides basic weather-related information.
package weather

// CurrentCondition represents the current weather condition.
var CurrentCondition string

// CurrentLocation represents the user's current location.
var CurrentLocation string

// Forecast returns a string describing the provided weather conditions for the location provided.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
