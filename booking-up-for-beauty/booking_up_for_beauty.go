package booking

import (
	"fmt"
	"log"
	"time"
)

// Schedule returns a time.Time from a string containing a date
func Schedule(date string) time.Time {
	parsedDate, err := time.Parse("1/2/2006 15:04:05", date)

	if err != nil {
		log.Fatal(err)
	}

	return parsedDate
}

// HasPassed returns whether a date has passed
func HasPassed(date string) bool {
	handledDate, err := time.Parse("January 2, 2006 15:04:05", date)

	if err != nil {
		log.Fatal(err)
	}

	timeDelta := time.Since(handledDate)

	return timeDelta > 0
}

// IsAfternoonAppointment returns whether a time is in the afternoon
func IsAfternoonAppointment(date string) bool {
	handledDate, err := time.Parse("Monday, January 2, 2006 15:04:05", date)

	if err != nil {
		log.Fatal(err)
	}

	return handledDate.Hour() > 11 && handledDate.Hour() < 18
}

// Description returns a formatted string of the appointment time
func Description(date string) string {
	handledDate := Schedule(date)

	return fmt.Sprintf("You have an appointment on %v, %v %v, %v, at %v:%v.",
		handledDate.Weekday(), handledDate.Month(), handledDate.Day(), handledDate.Year(), handledDate.Hour(), handledDate.Minute(),
	)
}

// AnniversaryDate returns a Time with this year's anniversary
func AnniversaryDate() time.Time {
	currentYear := time.Now().Year()
	return time.Date(currentYear, 9, 15, 0, 0, 0, 0, time.UTC)
}
