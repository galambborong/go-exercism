package annalyn

// CanFastAttack can be executed only when the knight is sleeping
func CanFastAttack(knightIsAwake bool) bool {
	return !knightIsAwake
}

// CanSpy can be executed if at least one of the characters is awake
func CanSpy(knightIsAwake, archerIsAwake, prisonerIsAwake bool) bool {
	isAwakeStates := []bool{knightIsAwake, archerIsAwake, prisonerIsAwake}

	awakeCount := 0

	for _, isAwake := range isAwakeStates {
		if isAwake {
			awakeCount += 1
		}
	}

	return awakeCount != 0
}

// CanSignalPrisoner can be executed if the prisoner is awake and the archer is sleeping
func CanSignalPrisoner(archerIsAwake, prisonerIsAwake bool) bool {
	return prisonerIsAwake && !archerIsAwake
}

// CanFreePrisoner can be executed if the prisoner is awake and the other 2 characters are asleep
// or if Annalyn's pet dog is with her and the archer is sleeping
func CanFreePrisoner(knightIsAwake, archerIsAwake, prisonerIsAwake, petDogIsPresent bool) bool {
	if petDogIsPresent && !archerIsAwake {
		return true
	}

	guardsAreSleeping := !knightIsAwake && !archerIsAwake

	if !petDogIsPresent && prisonerIsAwake && guardsAreSleeping {
		return true
	}

	return false
}
