package raindrops

import (
	"strconv"
	"strings"
)

func Convert(number int) string {
	factors := []int{3, 5, 7}

	var stringBuilder []string

	for _, factor := range factors {
		if number%factor == 0 {
			switch factor {
			case 3:
				stringBuilder = append(stringBuilder, "Pling")
			case 5:
				stringBuilder = append(stringBuilder, "Plang")
			case 7:
				stringBuilder = append(stringBuilder, "Plong")
			}
		}
	}

	if len(stringBuilder) == 0 {
		return strconv.Itoa(number)
	}

	return strings.Join(stringBuilder, "")
}
