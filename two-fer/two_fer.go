// Package twofer
package twofer

import "fmt"

// ShareWith takes an optional name and outputs an appropriate message
func ShareWith(name string) string {
	switch len(name) > 0 {
	case true:
		return fmt.Sprintf("One for %v, one for me.", name)
	default:
		return "One for you, one for me."
	}
}
