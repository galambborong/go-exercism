package parsinglogfiles

import (
	"fmt"
	"regexp"
	"strings"
)

func IsValidLine(text string) bool {
	re := regexp.MustCompile(`^(\[ERR\]|\[FTL\]|\[INF\]|\[TRC\]|\[DBG\]|\[WRN\])\s.*$`)
	return re.MatchString(text)
}

func SplitLogLine(text string) []string {
	re := regexp.MustCompile(`<[~\*=-]*>`)
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	re := regexp.MustCompile(`(?i)".*password.*"`)
	count := 0

	for _, line := range lines {
		if re.MatchString(line) {
			count++
		}
	}

	return count
}

func RemoveEndOfLineText(text string) string {
	re := regexp.MustCompile(`end-of-line\d+`)
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	re1 := regexp.MustCompile(`\[[A-Z]{3}\]\s.*User\s+\w*\s`)

	var handledLines []string

	for _, line := range lines {
		if re1.MatchString(line) {
			username := getUser(line)
			msg := fmt.Sprintf("[USR] %v %v", username, line)
			handledLines = append(handledLines, msg)
		} else {
			handledLines = append(handledLines, line)
		}
	}

	return handledLines
}

func getUser(str string) string {

	initialSplit := strings.SplitAfter(str, "User ")
	username := strings.SplitAfter(initialSplit[1], " ")

	var res string

	for _, usr := range username {
		trimmedUsername := strings.Trim(usr, " ")
		if len(trimmedUsername) > 0 {
			return trimmedUsername
		}
	}

	return res
}
