package interest

const (
	negativeRate = 3.213
	smallRate    = 0.5
	mediumRate   = 1.621
	maximumRate  = 2.475
)

// InterestRate returns the interest rate for the provided balance.
func InterestRate(balance float64) float32 {
	switch {
	case balance < 0:
		return negativeRate
	case balance < 1000:
		return smallRate
	case balance < 5000:
		return mediumRate
	default:
		return maximumRate
	}
}

// Interest calculates the interest for the provided balance.
func Interest(balance float64) float64 {
	interestRate := InterestRate(balance)
	return balance * (float64(interestRate) / 100)
}

// AnnualBalanceUpdate calculates the annual balance update, taking into account the interest rate.
func AnnualBalanceUpdate(balance float64) float64 {
	return Interest(balance) + balance
}

// YearsBeforeDesiredBalance calculates the minimum number of years required to reach the desired balance:
func YearsBeforeDesiredBalance(balance, targetBalance float64) int {
	latestBalance := balance
	years := 0

	for {
		if latestBalance >= targetBalance {
			break
		}

		latestBalance = AnnualBalanceUpdate(latestBalance)
		years++
	}

	return years
}
