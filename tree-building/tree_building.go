package tree

import (
	"sort"
)

type Record struct {
	ID     int
	Parent int
	// feel free to add fields as you see fit
}

type Node struct {
	ID       int
	Children []*Node
	// feel free to add fields as you see fit
}

var outerMostNode = &Node{ID: 0}

func Build(records []Record) (*Node, error) {

	if len(records) == 0 {
		return nil, nil
	}

	currentParentId := 0
	nodeDepth := 0

	for currentParentId < len(records) {
		filteredRecords := []Record{}
		for _, record := range records {
			if record.Parent == currentParentId && record.ID > nodeDepth {
				filteredRecords = append(filteredRecords, record)
			}
		}

		sort.Slice(filteredRecords, func(i, j int) bool {
			return filteredRecords[i].ID < filteredRecords[j].ID
		})

		for _, rec := range filteredRecords {
			// outerMostNode.Children = append(outerMostNode.Children, &Node{ID: rec.ID})
			node := getNode(outerMostNode, rec, 0)
			node.Children = append(node.Children, &Node{ID: rec.ID})
		}

		currentParentId++
	}

	return outerMostNode, nil
}

func getNode(mainNode *Node, record Record, currentDepth int) *Node {
	switch {
	case currentDepth == 0:
		mainNode.Children = append(mainNode.Children, &Node{ID: record.ID})
		return mainNode
	default:
		return getNode(mainNode.Children[0], record, currentDepth+1)
	}
}
