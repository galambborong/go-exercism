package tree

import (
	"fmt"
	"sort"
)

type Record struct {
	ID     int
	Parent int
}

type Node struct {
	ID       int
	Children []*Node
}

func Build(records []Record) (*Node, error) {
	if len(records) == 0 {
		return nil, nil
	}

	if !hasRoot(records) {
		return nil, fmt.Errorf("no root node")
	}

	rootNode := &Node{}
	if isSingleRootRecord(records) {
		return rootNode, nil
	}

	if !isContinuous(records) {
		return nil, fmt.Errorf("non-continuous")
	}

	highestParent, _ := getHighestIds(records)
	currentParentId := 0

	for currentParentId <= highestParent {
		err := buildNodeByParentId(rootNode, records, currentParentId)
		if err != nil {
			return nil, err
		}
		currentParentId++
	}

	return rootNode, nil
}

func isSingleRootRecord(records []Record) bool {
	return len(records) == 1 && records[0].ID == 0 && records[0].Parent == 0
}

func hasRoot(records []Record) bool {
	for _, record := range records {
		if record.ID == 0 && record.Parent == 0 {
			return true
		}
	}
	return false
}

func isContinuous(records []Record) bool {
	sortedRecords := sortRecords(records)
	for idx, record := range sortedRecords {
		if record.ID != idx {
			return false
		}
	}
	return true
}

func getHighestIds(records []Record) (int, int) {
	highestParent, highestId := 0, 0
	for _, record := range records {
		if record.Parent > highestParent {
			highestParent = record.Parent
		}

		if record.ID > highestId {
			highestId = record.ID
		}
	}

	return highestParent, highestId
}

func buildNodeByParentId(mainNode *Node, records []Record, parentId int) error {
	childRecords := []Record{}

	for _, record := range records {
		if record.Parent == parentId {
			childRecords = append(childRecords, record)
		}
	}

	childRecords = sortRecords(childRecords)

	if !isValidParentId(childRecords) {
		return fmt.Errorf("parent id is higher than id")
	}

	for _, record := range childRecords {
		if record.Parent == 0 && record.ID == 0 {
			continue
		} else if record.ID == record.Parent {
			return fmt.Errorf("cycle directly")
		} else {
			initialDepth := parentId
			node, _ := getNode(mainNode, initialDepth, parentId)
			node.Children = append(node.Children, &Node{ID: record.ID})
		}
	}

	return nil
}

func sortRecords(records []Record) []Record {
	sort.Slice(records, func(i, j int) bool {
		return records[i].ID < records[j].ID
	})
	return records
}

func isValidParentId(records []Record) bool {
	hiParentId, hiId := getHighestIds(records)
	return hiParentId < hiId
}

func getNode(node *Node, depth, desiredParentID int) (*Node, int) {
	if depth == 0 {
		return node, depth
	}

	if len(node.Children) == 0 {
		return node, depth
	}

	if node.ID == desiredParentID {
		return node, depth
	}

	if node.Children[0].ID < desiredParentID && len(node.Children) != 1 {
		return getNode(node.Children[1], depth-1, desiredParentID)
	}

	return getNode(node.Children[0], depth-1, desiredParentID)
}
