package lasagna

func PreparationTime(layers []string, time int) int {

	if time == 0 {
		time = 2
	}

	averagePrepTime := len(layers) * time

	return averagePrepTime
}

func Quantities(quantities []string) (noodles int, sauce float64) {
	sumNoodles := 0
	sumSauce := 0.0

	for _, item := range quantities {
		if item == "noodles" {
			sumNoodles += 50
		}
		if item == "sauce" {
			sumSauce += 0.2
		}
	}

	return sumNoodles, sumSauce
}

func AddSecretIngredient(theirIngredients []string, myIngredients []string) {
	myIngredients[len(myIngredients)-1] = theirIngredients[len(theirIngredients)-1]
}

func ScaleRecipe(quantities []float64, scale int) []float64 {
	var scaledQuantities []float64

	for _, quantity := range quantities {
		scaledQuantities = append(scaledQuantities, scaleQuantity(quantity, scale))
	}

	return scaledQuantities
}

func scaleQuantity(value float64, scale int) float64 {
	return (value / 2.0) * float64(scale)
}
