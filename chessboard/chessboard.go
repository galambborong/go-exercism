package chessboard

type Rank []bool

type Chessboard map[string]Rank

// CountInRank returns how many squares are occupied in the chessboard,
// within the given rank
func CountInRank(cb Chessboard, rank string) int {
	rankValue, exists := cb[rank]

	if !exists {
		return 0
	}

	totalOccupiedInRank := 0

	for _, isOccupied := range rankValue {
		if isOccupied {
			totalOccupiedInRank++
		}
	}

	return totalOccupiedInRank
}

// CountInFile returns how many squares are occupied in the chessboard,
// within the given file
func CountInFile(cb Chessboard, file int) int {
	isValid := file < 1 || file > 8
	isOccupied := file - 1

	if isValid {
		return 0
	}

	totalOccupiedInFile := 0

	for _, rank := range cb {
		if rank[isOccupied] {
			totalOccupiedInFile++
		}
	}

	return totalOccupiedInFile
}

// CountAll should count how many squares are present in the chessboard
func CountAll(cb Chessboard) int {
	totalSquares := 0

	for range cb {
		totalSquares++
	}

	return totalSquares * totalSquares
}

// CountOccupied returns how many squares are occupied in the chessboard
func CountOccupied(cb Chessboard) int {
	totalOccupied := 0

	for rank := range cb {
		totalOccupied += CountInRank(cb, rank)
	}

	return totalOccupied
}
