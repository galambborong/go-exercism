package logs

import (
	"strings"
	"unicode/utf8"
)

const (
	recommendation, recommendationStr = '❗', "recommendation"
	search, searchStr                 = '🔍', "search"
	weather, weatherStr               = '☀', "weather"
)

// Application identifies the application emitting the given log.
func Application(log string) string {
	for _, char := range log {
		switch char {
		case recommendation:
			return recommendationStr
		case search:
			return searchStr
		case weather:
			return weatherStr
		}
	}
	return "default"
}

// Replace replaces all occurrences of old with new, returning the modified log
// to the caller.
func Replace(log string, oldRune, newRune rune) string {
	return strings.ReplaceAll(log, string([]rune{oldRune}), string([]rune{newRune}))
}

// WithinLimit determines whether or not the number of characters in log is
// within the limit.
func WithinLimit(log string, limit int) bool {
	return utf8.RuneCountInString(log) <= limit
}
