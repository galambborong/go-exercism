package thefarm

import (
	"errors"
	"fmt"
)

type SillyNephewError struct {
	cowCount int
}

func (e *SillyNephewError) Error() string {
	return fmt.Sprintf("silly nephew, there cannot be %v cows", e.cowCount)
}

var ErrNegativeAmount = errors.New("negative fodder")
var ErrDivisionByZero = errors.New("division by zero")
var ErrNonScale = errors.New("non-scale error")

// DivideFood computes the fodder amount per cow for the given cows.
func DivideFood(weightFodder WeightFodder, cows int) (float64, error) {

	if cows == 0 {
		return 0, ErrDivisionByZero
	}

	if cows < 0 {
		err := SillyNephewError{cowCount: cows}
		return 0, errors.New(err.Error())
	}

	fodderAmount, err := weightFodder.FodderAmount()

	if fodderAmount < 0 {
		errIsScaleMalfunctionOrNil := err == ErrScaleMalfunction || err == nil

		if errIsScaleMalfunctionOrNil {
			return 0, ErrNegativeAmount
		}

		if err != nil {
			return 0, ErrNonScale
		}
	}

	if err == ErrScaleMalfunction {
		doubleAmount := fodderAmount * 2
		divisionAmount := doubleAmount / float64(cows)
		return divisionAmount, nil
	}

	if err != nil {
		return 0, err
	}

	return fodderAmount / float64(cows), nil
}
