package expenses

import (
	"fmt"
)

// Record represents an expense record.
type Record struct {
	Day      int
	Amount   float64
	Category string
}

// DaysPeriod represents a period of days for expenses.
type DaysPeriod struct {
	From int
	To   int
}

// Filter returns the records for which the predicate function returns true.
func Filter(in []Record, predicate func(Record) bool) []Record {
	var results []Record
	for _, record := range in {
		if predicate(record) {
			results = append(results, record)
		}
	}
	return results
}

// ByDaysPeriod returns predicate function that returns true when
// the day of the record is inside the period of day and false otherwise
func ByDaysPeriod(p DaysPeriod) func(Record) bool {
	return func(r Record) bool {
		dayIsWithinPeriod := r.Day >= p.From && r.Day <= p.To
		return dayIsWithinPeriod
	}
}

// ByCategory returns predicate function that returns true when
// the category of the record is the same as the provided category
// and false otherwise
func ByCategory(c string) func(Record) bool {
	return func(r Record) bool {
		return r.Category == c
	}
}

// TotalByPeriod returns total amount of expenses for records
// inside the period p
func TotalByPeriod(in []Record, p DaysPeriod) float64 {
	var total float64
	isWithinPeriod := ByDaysPeriod(p)
	for _, record := range in {
		if isWithinPeriod(record) {
			total += record.Amount
		}
	}
	return total
}

// CategoryExpenses returns total amount of expenses for records
// in category c that are also inside the period p.
// An error must be returned only if there are no records in the list that belong
// to the given category, regardless of period of time.
func CategoryExpenses(in []Record, p DaysPeriod, c string) (float64, error) {
	var total float64
	isWithinPeriod := ByDaysPeriod(p)
	isCategory := ByCategory(c)
	categoryExists := false
	errUnknownCategory := fmt.Errorf("unknown category %v", c)

	for _, record := range in {
		if isCategory(record) {
			categoryExists = true
			if isWithinPeriod(record) {
				total += record.Amount
			}
		}
	}

	if !categoryExists {
		return total, errUnknownCategory
	}

	return total, nil
}
