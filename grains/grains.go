package grains

import (
	"errors"
)

var valuesPerSquare = make(map[int]uint64)

func Square(number int) (uint64, error) {
	if number < 1 || number > 64 {
		return 0, errors.New("number does not fit within chessboard")
	}

	for i := 1; i <= number; i++ {
		if i == 1 {
			valuesPerSquare[i] = 1
		} else {
			valuesPerSquare[i] = valuesPerSquare[i-1] * 2
		}
	}

	return valuesPerSquare[number], nil
}

func Total() uint64 {
	_, err := Square(64)

	if err != nil {
		return 0
	}

	var totalSum uint64
	for _, value := range valuesPerSquare {
		totalSum += value
	}

	return totalSum
}
