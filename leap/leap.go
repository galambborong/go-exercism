package leap

func IsLeapYear(year int) bool {
	isDivisableBy4 := year%4 == 0
	isNotDivisableBy100 := year%100 != 0
	isDivisibleBy400 := year%400 == 0

	if isDivisableBy4 {
		if isNotDivisableBy100 || isDivisibleBy400 {
			return true
		}
	}
	return false
}
