package elon

import "fmt"

// Drive updates the car's battery and distance state
func (c *Car) Drive() {

	if c.battery < c.batteryDrain {
		return
	}

	c.battery -= c.batteryDrain
	c.distance += c.speed
}

// DisplayDistance returns the car's total distance driven in meters
func (c *Car) DisplayDistance() string {
	return fmt.Sprintf("Driven %v meters", c.distance)
}

// DisplayBattery displays the car's current battery level
func (c *Car) DisplayBattery() string {
	return fmt.Sprintf("Battery at %v%%", c.battery)
}

// CanFinish returns whether or not the car has enough battery to traverse the track distance
func (c *Car) CanFinish(trackDistance int) bool {
	driveUnits := trackDistance / c.speed
	batteryRemainingAtEndOfTrack := c.battery / driveUnits
	return batteryRemainingAtEndOfTrack >= c.batteryDrain
}
